//  OlarisAssets.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation

#if os(macOS)
import AppKit
#else
import UIKit
#endif

struct OlarisAssets {
	#if os(macOS)
	typealias Color = NSColor
	#else
	typealias Color = UIColor
	#endif

	static let background: Color = Color(named: "BackgroundColor")!
	static let text: Color = Color(named: "TextColor")!

	static let navigationBackground: Color = Color(named: "NavigationBackgroundColor")!
	static let navigationUnselected: Color = Color(named: "NavigationUnselectedColor")!
	static let navigationHighlighted: Color = Color(named: "NavigationHighlightedColor")!
}

extension OlarisAssets {
	static var standardNavigationAppearance: UINavigationBarAppearance {
		let standardNavAppearance = UINavigationBarAppearance()
		standardNavAppearance.configureWithOpaqueBackground()
		standardNavAppearance.backgroundColor = OlarisAssets.navigationBackground
		standardNavAppearance.titleTextAttributes = [.foregroundColor: OlarisAssets.navigationUnselected]
		return standardNavAppearance
	}

	static var largeTitleNavigationAppearance: UINavigationBarAppearance {
		let largeTitleNavAppearance = UINavigationBarAppearance()
		largeTitleNavAppearance.configureWithTransparentBackground()
		largeTitleNavAppearance.largeTitleTextAttributes = [.foregroundColor: OlarisAssets.navigationUnselected]
		return largeTitleNavAppearance
	}
}
