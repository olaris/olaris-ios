//
//  OlarisServer.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

struct OlarisServer {
	let baseURL: URL
	let session: URLSession

	enum CheckServerError: Error {
		case invalidURL
		case serverNotFound(URL)
		case networkError(URLError)
	}

	static func graphqlURL(baseURL: URL) -> URL? {
		URL(string: "/olaris/m/query", relativeTo: baseURL)
	}

	var graphqlURL: URL? {
		OlarisServer.graphqlURL(baseURL: baseURL)
	}

	static func authRequest(baseURL: URL, username: String, password: String) -> URLRequest? {
		guard let url = URL(string: "/olaris/m/v1/auth", relativeTo: baseURL) else {
			return nil
		}
		do {
			var request = URLRequest(url: url)
			request.httpMethod = "POST"
			let body: [String: String] = ["username": username, "password": password]
			request.httpBody = try JSONEncoder().encode(body)
			request.addValue("application/json", forHTTPHeaderField: "Content-Body")
			return request
		} catch {
			return nil
		}
	}

	static func checkAndCreate(baseURL: URL, session: URLSession = URLSession(configuration: .ephemeral)) -> AnyPublisher<OlarisServer, CheckServerError> {
		guard let graphqlURL = graphqlURL(baseURL: baseURL) else {
			return Fail(error: CheckServerError.invalidURL).eraseToAnyPublisher()
		}

		return session.dataTaskPublisher(for: graphqlURL)
			.mapError({ urlError in CheckServerError.networkError(urlError) })
			.flatMap { result -> AnyPublisher<OlarisServer, CheckServerError> in

				let (_, response) = result
				if (response as? HTTPURLResponse)?.statusCode == 404 {
					return Fail(outputType: OlarisServer.self, failure: CheckServerError.serverNotFound(baseURL))
						.eraseToAnyPublisher()
				}

				return Just(OlarisServer(baseURL: baseURL, session: session))
					.setFailureType(to: CheckServerError.self)
					.eraseToAnyPublisher()

			}
			.eraseToAnyPublisher()
	}

		
	init(baseURL: URL, session: URLSession = URLSession(configuration: .ephemeral)) {
		self.baseURL = baseURL
		self.session = session
	}

	enum JWTError: Error {
		case noPassword
		case cannotCreateAuthRequest
		case JSONError(Error)
		case networkError(URLError)
	}

	func fetchJWT(user: OlarisUser) -> AnyPublisher<String, JWTError> {
		struct JWT: Decodable {
			let jwt: String
		}

		guard let password = user.password else {
			return Fail(outputType: String.self, failure: JWTError.noPassword).eraseToAnyPublisher()
		}
		let username = user.username
		guard let request = OlarisServer.authRequest(baseURL: baseURL, username: username, password: password) else {
			return Fail(outputType: String.self, failure: JWTError.cannotCreateAuthRequest).eraseToAnyPublisher()
		}

		return session.dataTaskPublisher(for: request)
			.mapError({ urlError in JWTError.networkError(urlError) })
			.flatMap { result -> AnyPublisher<String, JWTError> in
				let (data, _) = result
				do {
					let jwt = try JSONDecoder().decode(JWT.self, from: data)
					return Just(jwt.jwt).setFailureType(to: JWTError.self).eraseToAnyPublisher()
				}
				catch let error {
					return Fail(outputType: String.self, failure: JWTError.JSONError(error)).eraseToAnyPublisher()
				}
			}
			.eraseToAnyPublisher()
	}
}
