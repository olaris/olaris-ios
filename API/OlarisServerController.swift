//  OlarisServerController.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

class OlarisServerController: ObservableObject {
	enum Status {
		case loggedOut
		case loggedIn(OlarisServer, OlarisUser, GraphQLClient)
	}

	let objectWillChange = ObservableObjectPublisher()

	private(set) var status: Status {
		didSet {
			objectWillChange.send()
		}
	}

	init() {
		self.status = .loggedOut
	}

	func url(path: String) -> URL? {
		switch status {
		case .loggedIn(_, let user, _):
			let baseURL = user.server.flatMap { URL(string: $0) }
			return baseURL.flatMap { URL(string: path, relativeTo: $0) }
		case .loggedOut:
			return nil
		}
	}

	func logIn(server: OlarisServer, user: OlarisUser, client: GraphQLClient) {
		status = .loggedIn(server, user, client)
	}

	func logOut() {
		status = .loggedOut
	}

	var isLoggedOut: Bool {
		switch status {
		case .loggedOut:
			return true
		default:
			return false
		}
	}

	var graphqlClient: GraphQLClient? {
		switch status {
		case .loggedIn(_, _, let client):
			return client
		default:
			return nil
		}
	}

	var cancellable: AnyCancellable? = nil
	func attemptLogin(server: String, username: String, password: String) {
		guard
			let user = OlarisUser(username: username, password: password, server: server),
			let serverURL = URL(string: server) else {
			return
		}

		cancellable = OlarisServer.checkAndCreate(baseURL: serverURL)
			.ignoreErrors()
			.flatMap { server in
				server.fetchJWT(user: user).ignoreErrors()
					.compactMap { jwt in
						GraphQLClient(server: server, jwt: jwt)
					}
				.map { client in
					Status.loggedIn(server, user, client)
				}
			}
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { _ in
				self.cancellable = nil
			}) { status in
				self.status = status
			}
	}
}
