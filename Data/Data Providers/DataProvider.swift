//  DataProvider.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

protocol DataProvider: ObservableObject where ObjectWillChangePublisher.Output == Status {
	associatedtype ObjectType
	associatedtype ErrorType: Error

	typealias Status = DataProviderStatus<ObjectType, ErrorType>
	var status: Status { get }

	func fetch()
}

public enum DataProviderStatus<ObjectType, ErrorType> {
	case unloaded
	case loading(ObjectType?)
	case loaded(ObjectType)
	case failed(ObjectType?, ErrorType)

	var name: String {
		switch self {
		case .unloaded:
			return ".unloaded"
		case .loading(_):
			return ".loading"
		case .loaded(_):
			return ".loaded"
		case .failed(_, _):
			return ".failed"
		}
	}

	var data: ObjectType? {
		switch self {
		case .unloaded:
			return nil
		case .loading(let data):
			return data
		case .loaded(let data):
			return data
		case .failed(let data, _):
			return data
		}
	}

	var failed: ErrorType? {
		switch self {
		case .failed(_, let error):
			return error
		default:
			return nil
		}
	}

	var loading: Bool {
		if case .loading = self {
			return true
		}
		else {
			return false
		}
	}
}
