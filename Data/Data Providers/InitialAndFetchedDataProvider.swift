//  InitialAndFetchedDataProvider.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class InitialAndFetchedDataProvider<Initial, Provider: DataProvider>: DataProvider {
	enum Result {
		case initial(Initial)
		case fetched(Provider.ObjectType)
	}

	typealias ObjectType = Result
	typealias ErrorType = Provider.ErrorType

	let initial: Initial
	let provider: Provider

	init(initial: Initial, provider: Provider) {
		self.initial = initial
		self.provider = provider
		fetchedStatus = provider.status

		cancellable = provider.objectWillChange
			.assign(to: \.fetchedStatus, on: self)
	}

	private var fetchedStatus: Provider.Status
	private var cancellable: AnyCancellable?

	private func mappedStatus(_ status: Provider.Status) -> Status {
		func result(_ fetched: Provider.ObjectType?) -> Result {
			if let fetch = fetched {
				return .fetched(fetch)
			}
			else {
				return .initial(initial)
			}
		}

		switch status {
		case .unloaded:
			return .loaded(.initial(initial))
		case .loading(let fetched):
			return .loading(result(fetched))
		case .loaded(let fetched):
			return .loaded(result(fetched))
		case .failed(let fetched, let error):
			return .failed(result(fetched), error)
		}
	}

	var status: DataProviderStatus<InitialAndFetchedDataProvider<Initial, Provider>.Result, Provider.ErrorType> {
		mappedStatus(fetchedStatus)
	}

	func fetch() {
		provider.fetch()
	}

	typealias ObjectWillChangePublisher = AnyPublisher<Status, Never>
	lazy var objectWillChange: ObjectWillChangePublisher = {
		provider.objectWillChange
			.map(mappedStatus)
			.eraseToAnyPublisher()
	}()
}

extension DataProvider {
	func withInitialValue<T>(value: T) -> InitialAndFetchedDataProvider<T, Self> {
		InitialAndFetchedDataProvider(initial: value, provider: self)
	}
}
