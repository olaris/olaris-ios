//  OptionalFilteringDataProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class OptionalFilteringDataProvider<T, Provider: DataProvider>: DataProvider where Provider.ObjectType: Collection, Provider.ObjectType.Element == Optional<T> {
	typealias ObjectWillChangePublisher = AnyPublisher<DataProviderStatus<ObjectType, Provider.ErrorType> , Never>
	var objectWillChange: AnyPublisher<DataProviderStatus<ObjectType, ErrorType> , Never> = Empty().eraseToAnyPublisher()

	private let statusSubject = PassthroughSubject<Status, Never>()
	var status: DataProviderStatus<ObjectType, ErrorType> = .unloaded

	typealias ObjectType = Array<T>
	typealias ErrorType = Provider.ErrorType

	let provider: Provider

	init(provider: Provider) {
		self.provider = provider

		objectWillChange = provider.objectWillChange
			.map({ input -> DataProviderStatus<ObjectType, ErrorType> in
				switch provider.status {
				case .unloaded:
					return .unloaded
				case .loading(let collection):
					return .loading(collection?.compactMap { $0 })
				case .loaded(let collection):
					return .loaded(collection.compactMap { $0 })
				case .failed(let collection, let error):
					return .failed(collection?.compactMap { $0 }, error)
				}
			})
			.handleEvents(receiveOutput: { status in
				self.status = status
			})
			.eraseToAnyPublisher()
	}

	func fetch() {
		provider.fetch()
	}
}

extension DataProvider where Self.ObjectType: Collection {
	func filteringOptionals<T>() -> OptionalFilteringDataProvider<T, Self> {
		OptionalFilteringDataProvider(provider: self)
	}
}
