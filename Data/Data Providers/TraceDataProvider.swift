//  TraceDataProvider.swift
//  Olaris
//
//  Copyright (c) 2021 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class TraceDataProvider<Provider: DataProvider>: DataProvider {
	typealias ObjectType = Provider.ObjectType
	typealias ErrorType = Provider.ErrorType
	typealias Status = DataProviderStatus<ObjectType, ErrorType>
	typealias ObjectWillChangePublisher = AnyPublisher<Status, Never>

	var objectWillChange: AnyPublisher<Status, Never> {
		provider.objectWillChange
			.handleEvents(
				receiveSubscription: receiveSubscription,
				receiveOutput: receiveOutput,
				receiveCompletion: receiveCompletion,
				receiveCancel: receiveCancel,
				receiveRequest: receiveRequest
			)
			.eraseToAnyPublisher()
	}

	var status: Status {
		provider.status
	}

	func fetch() {
		provider.fetch()
	}

	let provider: Provider
	let receiveSubscription: ((Subscription) -> Void)?
	let receiveOutput: ((Provider.Status) -> Void)?
	let receiveCompletion: ((Subscribers.Completion<Never>) -> Void)?
	let receiveCancel: (() -> Void)?
	let receiveRequest: ((Subscribers.Demand) -> Void)?

	init(
		provider: Provider,
		receiveSubscription: ((Subscription) -> Void)? = nil,
		receiveOutput: ((Provider.Status) -> Void)? = nil,
		receiveCompletion: ((Subscribers.Completion<Never>) -> Void)? = nil,
		receiveCancel: (() -> Void)? = nil,
		receiveRequest: ((Subscribers.Demand) -> Void)? = nil
	) {
		self.provider = provider
		self.receiveSubscription = receiveSubscription
		self.receiveOutput = receiveOutput
		self.receiveCompletion = receiveCompletion
		self.receiveCancel = receiveCancel
		self.receiveRequest = receiveRequest
	}
}

extension DataProvider {
	func tracing(
		receiveSubscription: ((Subscription) -> Void)? = nil,
		receiveOutput: ((Self.Status) -> Void)? = nil,
		receiveCompletion: ((Subscribers.Completion<Never>) -> Void)? = nil,
		receiveCancel: (() -> Void)? = nil,
		receiveRequest: ((Subscribers.Demand) -> Void)? = nil
	) -> TraceDataProvider<Self> {
		TraceDataProvider(
			provider: self,
			receiveSubscription: receiveSubscription,
			receiveOutput: receiveOutput,
			receiveCompletion: receiveCompletion,
			receiveCancel: receiveCancel,
			receiveRequest: receiveRequest
		)
	}
}
