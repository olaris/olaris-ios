//  AnySectionProvider.swift
//  Olaris-Mac
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class AnySectionProvider<T, E>: SectionProvider {
	typealias ObjectType = T
	typealias ObjectChanged = E

	var numberOfSections: Int {
		getNumberOfSections()
	}

	func numberOfItems(in section: Int) -> Int {
		getNumberOfItems(section)
	}

	func item(at indexPath: IndexPath, full fullIndexPath: IndexPath) -> ObjectType? {
		getItem(indexPath, fullIndexPath)
	}

	private let getNumberOfSections: () -> Int
	private let getNumberOfItems: (Int) -> Int
	private let getItem: (IndexPath, IndexPath) -> ObjectType?
	private let getObjectWillChange: () -> AnyPublisher<ObjectChanged, Never>
	private let getUpdateNewerIfNeeded: () -> Void
	private let getIsUpdating: () -> Bool

	init<P: SectionProvider>(sectionProvider: P) where P.ObjectType == ObjectType, P.ObjectWillChangePublisher.Output == ObjectChanged {
		getNumberOfSections = { () in sectionProvider.numberOfSections }
		getNumberOfItems = sectionProvider.numberOfItems(in:)
		getItem = sectionProvider.item(at:full:)
		getObjectWillChange = { () in sectionProvider.objectWillChange.eraseToAnyPublisher() }
		getUpdateNewerIfNeeded = sectionProvider.updateNewerIfNeeded
		getIsUpdating = { () in sectionProvider.updating }
	}

	func updateNewerIfNeeded() {
		getUpdateNewerIfNeeded()
	}

	var updating: Bool {
		getIsUpdating()
	}
}

extension SectionProvider {
	var asAnySectionProvider: AnySectionProvider<Self.ObjectType, Self.ObjectWillChangePublisher.Output> {
		AnySectionProvider(sectionProvider: self)
	}
}
