//  SectionProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

protocol SectionProvider: ObservableObject {
	associatedtype ObjectType

	var numberOfSections: Int { get }
	func numberOfItems(in section: Int) -> Int
	func item(at indexPath: IndexPath, full fullndexPath: IndexPath) -> ObjectType?

	func updateNewerIfNeeded()
	var updating: Bool { get }
}

extension SectionProvider {
	var sectionIdentifiers: [Int] {
		Array(0..<numberOfSections)
	}

	func itemIdentifiers(in section: Int) -> [Self.ObjectType]? {
		Array(0..<numberOfItems(in: section))
			.map { IndexPath(item: $0, section: section) }
			.compactMap { indexPath in item(at: indexPath, full: indexPath) }
	}

	var allItems: [[Self.ObjectType]] {
		sectionIdentifiers.map { section in
			itemIdentifiers(in: section) ?? []
		}
	}
}
