//  CollectionViewPublisherDelegate.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import UIKit

final class CollectionViewPublisherDelegate: NSObject, UICollectionViewDelegate {
	let scrollViewDelegate: ScrollViewPublisherDelegate

	enum SelectedState {
		case none
		case highlighted
		case selected
	}
	private var selectedSubject = PassthroughSubject<(IndexPath, SelectedState), Never>()
	lazy var selected: AnyPublisher<(IndexPath, SelectedState), Never> = ({
		selectedSubject.eraseToAnyPublisher()
	})()

	init(collectionView: UICollectionView) {
		scrollViewDelegate = ScrollViewPublisherDelegate(scrollView: collectionView)
	}

	func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
		selectedSubject.send((indexPath, .highlighted))
	}

	func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
		let selected = collectionView.indexPathsForSelectedItems?.contains(indexPath) ?? false
		selectedSubject.send((indexPath, selected ? .selected : .none))
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		selectedSubject.send((indexPath, .selected))
	}

	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
		selectedSubject.send((indexPath, .none))
	}

	// MARK: ScrollView
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewDidScroll(scrollView)
	}

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewWillBeginDragging(scrollView)
	}

	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		scrollViewDelegate.scrollViewDidEndDragging(scrollView, willDecelerate: decelerate)
	}

	func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewWillBeginDecelerating(scrollView)
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewDidEndDecelerating(scrollView)
	}

	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewDidEndScrollingAnimation(scrollView)
	}

	func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewDidScrollToTop(scrollView)
	}

	func scrollViewDidChangeAdjustedContentInset(_ scrollView: UIScrollView) {
		scrollViewDelegate.scrollViewDidChangeAdjustedContentInset(scrollView)
	}
}
