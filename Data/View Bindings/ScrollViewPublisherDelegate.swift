//  ScrollViewPublishers.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import UIKit

final class ScrollViewPublisherDelegate: NSObject, UIScrollViewDelegate {
	struct ScrollState {
		let offset: CGPoint
		let size: CGSize
		let bounds: CGRect
		let isDragging: Bool
		let isDecelerating: Bool

		var distanceFromBottom: CGFloat {
			size.height - offset.y
		}

		init(scrollView: UIScrollView) {
			self.init(
				offset: scrollView.contentOffset,
				size: scrollView.contentSize,
				bounds: scrollView.bounds,
				isDragging: scrollView.isDragging,
				isDecelerating: scrollView.isDecelerating
			)
		}

		init(offset: CGPoint, size: CGSize, bounds: CGRect, isDragging: Bool, isDecelerating: Bool) {
			self.offset = offset
			self.size = size
			self.bounds = bounds
			self.isDragging = isDragging
			self.isDecelerating = isDecelerating
		}
	}

	private let scrollStateSubject: CurrentValueSubject<ScrollState, Never>
	lazy var scrollState = ({
		scrollStateSubject.eraseToAnyPublisher()
	})()

	init(scrollView: UIScrollView) {
		let initialState = ScrollState(scrollView: scrollView)
		scrollStateSubject = CurrentValueSubject(initialState)

		super.init()
	}

	private func update(_ scrollView: UIScrollView) {
		scrollStateSubject.send(ScrollState(scrollView: scrollView))
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		update(scrollView)
	}

	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		update(scrollView)
	}

	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		update(scrollView)
	}

	func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
		update(scrollView)
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		update(scrollView)
	}

	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		update(scrollView)
	}

	func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
		update(scrollView)
	}

	func scrollViewDidChangeAdjustedContentInset(_ scrollView: UIScrollView) {
		update(scrollView)
	}
}

extension ScrollViewPublisherDelegate: UICollectionViewDelegate, UITableViewDelegate {
}
