//
//  AppDelegate.swift
//  Olaris-TV
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
import SwiftUI
import Combine

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

	let serverController = OlarisServerController()
	var cancellables: [AnyCancellable] = []

	let useSwiftUI: Bool = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

		guard OlarisUser.canAccessKeychain else {
			assertionFailure()
			return false
		}

		if let user = OlarisUser.all.first, let (_, _, server) = user.credentials, let url = URL(string: server) {
			let server = OlarisServer(baseURL: url)
			cancellables.append(server.fetchJWT(user: user)
				.receive(on: RunLoop.main)
				.sink(receiveCompletion: { _ in
					self.start()
				}) { jwt in
					if let client = GraphQLClient(server: server, jwt: jwt) {
						self.serverController.logIn(server: server, user: user, client: client)
					}
			})
		}
		else {
			start()
		}
		return true
	}

	func start() {
		let window = UIWindow(frame: UIScreen.main.bounds)
		if useSwiftUI {
			// Create the SwiftUI view that provides the window contents.
			let contentView = ContentView()
				.environmentObject(serverController)

			// Use a UIHostingController as window root view controller.
			window.rootViewController = UIHostingController(rootView: contentView)
		}
		else {
			let movies: UIViewController = ({
				let rootVC = MoviesCollectionViewController(serverController: serverController)
				let nav = UINavigationController(rootViewController: rootVC)
				return nav
			})()
//			let series: UIViewController = ({
//				let rootVC = SeriesCollectionViewController(serverController: serverController)
//				let nav = UINavigationController(rootViewController: rootVC)
//				nav.navigationBar.prefersLargeTitles = true
//				return nav
//			})()
			let tabBar = UITabBarController()
			tabBar.viewControllers = [
				movies,
//				series
			]
			window.rootViewController = tabBar
			tabBar.view.backgroundColor = OlarisAssets.background
			window.tintColor = .systemPurple

			let loggedIn = serverController.objectWillChange
				.prepend(())
				.map { self.serverController }
				.map(\.isLoggedOut)
				.removeDuplicates()
				.debounce(for: 0.1, scheduler: DispatchQueue.main)
				.scan((nil, nil)) { (input, loggedOut) -> (UIViewController?, UIViewController?) in
					let (_, previous) = input
					if loggedOut {
						let vc = UIHostingController(rootView: LoginSheet().environmentObject(self.serverController))
						vc.modalPresentationStyle = .overCurrentContext
						return (previous, vc)
					}
					else {
						return (previous, nil)
					}
			}
			.eraseToAnyPublisher()

			cancellables.append(loggedIn
				.sink(receiveValue: { value in
					let (previous, next) = value
					let showNext = { () -> Void in
						if let next = next {
							tabBar.present(next, animated: true, completion: nil)
						}
					}
					if let previous = previous {
						previous.dismiss(animated: true, completion: showNext)
					}
					else {
						showNext()
					}
				})
			)
		}
        self.window = window
        window.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }


}

