//  LoginSheet.swift
//  Olaris-iOS
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SwiftUI

struct LoginSheet: View {
	@EnvironmentObject var serverController: OlarisServerController
	@State var server: String = ""
	@State var username: String = ""
	@State var password: String = ""

	var canLogin: Bool {
		!(server.isEmpty || username.isEmpty || password.isEmpty)
	}

	var serverField: some View {
		let textField = TextField("https://my-olaris-server.local/", text: $server)
			.disableAutocorrection(true)
		#if os(iOS)
		return textField
			.keyboardType(.URL)
			.autocapitalization(.none)
			.textContentType(.URL)
		#else
		return textField
		#endif
	}

	var usernameField: some View {
		let textField = TextField("Username", text: $username)
			.disableAutocorrection(true)
		#if os(iOS)
		return textField
			.autocapitalization(.none)
			.textContentType(.username)
		#else
		return textField
		#endif
	}

	var passwordField: some View {
		let textField = SecureField("Password", text: $password)
			.disableAutocorrection(true)
		#if os(iOS)
		return textField
			.autocapitalization(.none)
			.textContentType(.password)
		#else
		return textField
		#endif
	}

	func loginButton(action: @escaping () -> Void) -> some View {
		let button = Button("Login", action: action)
			.disabled(!canLogin)
		#if os(macOS)
		return button
			.buttonStyle(BorderedButtonStyle())
		#else
		return button
		#endif
	}

	var loginButtonWithAction: some View {
		loginButton {
			self.serverController.attemptLogin(server: self.server, username: self.username, password: self.password)
		}
	}

	var form: some View {
		Form {
			Section(header: Text("Server").font(.headline)) {
				serverField
			}
			Section(header: Text("Credentials").font(.headline)) {
				usernameField
				passwordField
			}
			loginButtonWithAction
		}
	}

	var styledForm: some View {
		#if os(iOS)
		return NavigationView {
			form
				.navigationBarTitle("Log in to Olaris")
		}
		#elseif os(macOS)
		return form
			.padding(20)
			.touchBar {
				loginButtonWithAction
		}
		#elseif os(tvOS)
		return form
		#else
		return form
		#endif
	}

    var body: some View {
		styledForm
			.onAppear {
				print("Server controller: \(self.serverController)")
			}
    }
}
//
struct LoginSheet_Previews: PreviewProvider {
	static var previews: some View {
		LoginSheet()
    }
}
