//  DragHandleView.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit

final class DragHandleView: UIView {
	let blurEffectView = UIVisualEffectView(frame: .zero)
	let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemChromeMaterialLight)
	let vibrancyEffectView = UIVisualEffectView(frame: .zero)
	let vibrancyEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: UIBlurEffect.Style.systemChromeMaterialLight), style: .separator)
	let fillView = UIView(frame: .zero)

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	private func setup() {
		insertSubview(blurEffectView, at: 0)

		vibrancyEffectView.effect = vibrancyEffect
		blurEffectView.effect = blurEffect

		blurEffectView.contentView.addSubview(vibrancyEffectView)
		vibrancyEffectView.contentView.addSubview(fillView)

		backgroundColor = .clear
		isOpaque = false
		clipsToBounds = true

		blurEffectView.backgroundColor = .clear
		blurEffectView.isOpaque = false
		blurEffectView.clipsToBounds = true

		vibrancyEffectView.backgroundColor = .clear
		vibrancyEffectView.isOpaque = false
		vibrancyEffectView.clipsToBounds = true

		fillView.backgroundColor = .white
	}

	override var frame: CGRect {
		didSet {
			blurEffectView.frame = bounds
			vibrancyEffectView.frame = blurEffectView.bounds
			fillView.frame = vibrancyEffectView.bounds
			blurEffectView.layer.cornerRadius = bounds.size.height / 2
		}
	}
}
