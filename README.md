Olaris
======

[Olaris](https://gitlab.com/olaris/olaris-server) is an open-source, community driven, media manager and transcoding server. This repository contains applications for iOS, tvOS, and macOS for connecting to an Olaris server and accessing its media.

This is an ongoing project and should not be considered ready for use yet.

Developing
==========

Clone the repository and open the xcworkspace. There are three targets, `Olaris-Mac`, `Olaris-iOS`, and `Olaris-TV`. Building any of them will build the app for the appropriate platform.

- macOS requires Mac OS X 10.15 Catalina
- iOS requires iOS 13
- tvOS requires tvOS 13
- All clients require an Olaris server running 0.3.2 or higher

You may need to change the Team settings in Xcode to get the project to build. The project team is owned by [Steve Streza](https://gitlab.com/stevestreza), and regular contributors can request to be added to the developer program under this account if needed (but most contributions should be usable in a simulator).

Olaris uses [GraphQL](https://graphql.org/) to communicate with an Olaris server. All queries are added as `.graphql` files and converted to Swift code through code generation via the [Apollo-iOS](https://github.com/apollographql/apollo-ios) project. By adding a `.graphql` file with a query, mutation, or schema to the project, it will get generated into the `API.swift` file and will be available for use after the next compile.

License
=======

This project is licensed under the MIT License.

See [LICENSE.txt](LICENSE.txt) for more information.

Acknowledgements
================

Portions of this project have been developed on the traditional unceded land of the first people of Seattle, the Duwamish People past and present, and we honor with gratitude the land itself and the Duwamish Tribe, a people that are still here. We encourage financially privileged users of this project to support the Duwamish People by making a contribution to [Real Rent Duwamish](https://www.realrentduwamish.org/).

This project uses the following open source projects, and any open source projects they include:

- [Apollo](https://github.com/apollographql/apollo-ios)
- [URLImage](https://github.com/dmytro-anokhin/url-image)
- [GridStack](https://github.com/pietropizzi/GridStack)
- [Valet](https://github.com/Square/Valet)
- [VideoPlayer](https://github.com/wxxsw/VideoPlayer)
- [SDWebImage](https://sdwebimage.github.io/)
- [TONavigationBar](https://github.com/TimOliver/TONavigationBar)
